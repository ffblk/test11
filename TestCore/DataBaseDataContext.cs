﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using TestCore.Models;

namespace TestCore
{
    public class DataBaseDataContext
    {
        readonly private string connectPrices = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Prices.mdf";
        private string connectRealization = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\";
        public List<Prices> ListOfPrice { get; set; }
        public List<Realization> ListOfRealizations { get; set; }
        private bool IsInitialized = false;
        public DataBaseDataContext()
        {
            ListOfPrice = new List<Prices>();
            ListOfRealizations = new List<Realization>();
            if(IsInitialized!=true)
                CreateDB();
            GetPrices();
        }
        public void GetPrices()
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = connectPrices;
                connection.Open();
                var db = new DataContext(connectPrices);
                Table<Prices> Customers = db.GetTable<Prices>();
                foreach (var e in Customers)
                {
                    ListOfPrice.Add(e);
                }
                connection.Close();
            }
        }
        public IEnumerable<Prices> FindId(int findId)
        {
            return ListOfPrice.Where(pr => pr.Id == findId);
        }
        public IEnumerable<Prices> FindName(string findString)
        {
            return ListOfPrice.Where(pr => pr.Name.Contains(findString));
        }
        public IEnumerable<Prices> FindPrice(int findPrice)
        {
            return ListOfPrice.Where(pr => pr.Price == findPrice);
        }
        public string GetNameDB()
        {
            return "Realization"+DateTime.Today.Date.ToString("d").Replace("/", "").Replace(".", "");
        }
        public void Check(IEnumerable<Realization> pr)
        {
            string connect = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\" + GetNameDB() + ".mdf";
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = connect;
                connection.Open();
                var db = new DataContext(connect);
                foreach (var ee in pr)
                {
                    if(ee.Count>0)
                        db.GetTable<Realization>().InsertOnSubmit(ee);
                }
                db.SubmitChanges();
                connection.Close();
            }
        }
        public void CreateDB()
        {
            
            var dataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory");
            SqlConnection myConn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;");
            SqlConnection myConnTable = new SqlConnection(connectRealization+GetNameDB()+".mdf");
            var createDatabaseString =
                "IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = '" + GetNameDB() + "')" +
                " CREATE DATABASE " + GetNameDB() + " ON PRIMARY" +
                " (NAME = " + GetNameDB() + "_Data," +
                " FILENAME = '" + dataDirectory + "\\" + GetNameDB() + ".mdf', " +
                " SIZE = 3MB, FILEGROWTH = 10 %)" +
                " LOG ON (NAME = " + GetNameDB() + "_Log," +
                " FILENAME = '" + dataDirectory + "\\" + GetNameDB() + ".ldf', " +
                " SIZE = 1MB," +
                " FILEGROWTH = 10 %)";
            var drop = "DROP DATABASE "+GetNameDB()+";";
            var droptable = "DROP TABLE Realization;";
            var createTableString = "CREATE TABLE Realization ("+
                "Id INT NOT NULL PRIMARY KEY IDENTITY, "+
                "Product_Id INT NOT NULL,"+
                " Name VARCHAR(90) NOT NULL," +
                " Price INT NOT NULL," +
                " Count INT NOT NULL," +
                " Cost INT NOT NULL," +
                " Time TIME NOT NULL)";
            SqlCommand createDatabase = new SqlCommand(createDatabaseString, myConn);
            SqlCommand createTable = new SqlCommand(createTableString, myConnTable);
            string directoryFile = dataDirectory + "\\" + GetNameDB() + ".mdf";
            if (!File.Exists(directoryFile))
            {
                try
                {
                    myConn.Open();
                    createDatabase.ExecuteNonQuery();
                    if (myConn.State == ConnectionState.Open)
                    {
                        myConn.Close();
                    }
                    myConnTable.Open();
                    createTable.ExecuteNonQuery();
                }

                catch (System.Exception ex1)
                {
                    var errorMessage = ex1.Message;
                }
                finally
                {
                    if (myConnTable.State == ConnectionState.Open)
                    {
                        myConnTable.Close();
                    }
                }
            }
        }
        public void WriteXml(string findPrice)
        {
            var dataDirectory = AppDomain.CurrentDomain.GetData("DataDirectory");
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = connectRealization+GetNameDB()+".mdf";
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * from Realization WHERE Price >"+findPrice;
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int Id = reader.GetInt32(0);
                    int Product_Id = reader.GetInt32(1);
                    string Name = reader.GetString(2);
                    int Price = reader.GetInt32(3);
                    int Count = reader.GetInt32(4);
                    int Cost = reader.GetInt32(5);
                    DateTime Time = DateTime.Today.Add(reader.GetTimeSpan(6));
                    ListOfRealizations.Add(new Realization(Id, Product_Id, Name, Price, Count, Cost, Time));
                }
                connection.Close();
            }
            // The XML
            var xmlr = new XElement("repository",
                new XElement("Realizations",
                from pr in this.ListOfRealizations
                select new XElement("Project",
                    new XElement("Id", pr.Id),
                    new XElement("ProductId", pr.Product_Id),
                    new XElement("Name", pr.Name),
                    new XElement("Description", pr.Price),
                    new XElement("StartDate", pr.Count),
                    new XElement("EndDate", pr.Cost),
                    new XElement("Time", pr.Time)
                    )
                )
            );
            xmlr.Save(dataDirectory+"\\"+GetNameDB()+".xml");
        }

    }
}
