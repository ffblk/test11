﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCore.Models
{
    [Table(Name = "Prices")]
    public class Prices
    {
        private int _Id;
        private string _Name;
        private int _Price;
        private bool _Chosen;
        [Column(IsPrimaryKey = true,Name="Id", Storage = "_Id", DbType = "int NOT NULL IDENTITY")]
        public int Id {
            set { _Id = value; }
            get {return _Id;} }
        [Column(Storage = "_Name", Name="Name", DbType = "nvarchar(90) NOT NULL")]
        public string Name {
            set { _Name = value; } get { return _Name; } }
        [Column(Storage = "_Price",  Name = "Price", DbType = "int NOT NULL")]
        public int Price {
            set { _Price = value; } get { return _Price; } }
        [Column(Storage = "_Chosen", Name = "Chosen", DbType = "bit NOT NULL")]
        public bool Chosen {
            set { _Chosen = value; } get { return _Chosen; } }
    }
}
