﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Compilation;
using AutoMapper;
using TestUI.Models;

namespace TestUI.App_Start
{
    public class UIMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Models.Prices, TestCore.Models.Prices>();
            Mapper.CreateMap<TestCore.Models.Prices , Models.Prices>()
                .ForMember(desr => desr.Count,
                opts => opts.UseValue(0));
            Mapper.CreateMap<Realization, TestCore.Models.Realization>()
                .ForMember(dest => dest.Product_Id,
                opts => opts.MapFrom(src => src.Id));
            Mapper.CreateMap<TestCore.Models.Realization, Realization>()
                .ForMember(dest => dest.Id,
                opts => opts.MapFrom(src => src.Product_Id));
        }
    }
}